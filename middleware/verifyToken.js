const jwt = require("jsonwebtoken");

exports.verifyToken = (req, res, next) => {
  const token = req.headers["authorization"];
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    if (err instanceof jwt.JsonWebTokenError) {
      console.log(err);
      return res.error({code:401, message:"Please login to perform this action"})
    } else if (err) {
      return res.error({code:403, message:"Bad Request"});
    }
    req.payload = payload;
    console.log("payload",payload);
    next();
  });
};

exports.verifyEmployerToken = (req, res, next) => {
  const token = req.headers["authorization"];
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    console.log("payload",payload);
    if (err instanceof jwt.JsonWebTokenError) {
      return res.error({code:401, message:"Please login to perform this action"})
    } else if (err || payload.role !=='recruiter') {
      return res.error({code:403, message:"Bad Request"});
    }
    req.payload = payload;
    next();
  });
};

exports.verifyAdminToken = (req, res, next) => {
  const token = req.headers["authorization"];
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    console.log("payload",payload);
    if (err instanceof jwt.JsonWebTokenError) {
      return res.error({code:401, message:"Please login to perform this action"})
    } else if (err || payload.role !=='admin') {
      return res.error({code:403, message:"Bad Request"});
    }
    req.payload = payload;
    next();
  });
};

exports.verifyCandidateToken = (req, res, next) => {
  const token = req.headers["authorization"];
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    console.log("payload",payload);
    if (err instanceof jwt.JsonWebTokenError) {
      return res.error({code:401, message:"Please login to perform this action"})
    } else if (err || payload.role !=='seeker') {
      return res.error({code:403, message:"Bad Request"});

    }
    req.payload = payload;
    next();
  });
};

exports.getUserId = (req, res, next) => {
  const token = req.headers["authorization"];
  jwt.verify(token, process.env.SECRET_KEY, (err, payload) => {
    if (err instanceof jwt.JsonWebTokenError) {
      console.log(err);
      req.payload = {};
    } else {
      console.log(payload);
      req.payload = payload;
    }
    next();
  });
};