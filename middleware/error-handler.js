module.exports = errorHandler;
const { validate, ValidationError, Joi } = require("express-validation");

function errorHandler(err, req, res, next) {
  if (err instanceof ValidationError) {
    const extractedErrors = [];
    err.details.body.map(e=>{
        extractedErrors.push({
            field:e.path[0],
            message:e.message
        })
    })
    const error = {
      code: err.statusCode,
      message: "Validation Failed",
      errors: extractedErrors
    };
    // return res.status(err.statusCode).json(extra);
    return res.error({code:422, error:extractedErrors})
  }

  if (typeof err === "string") {
    obj = {
      code:400,
      error:err
    }
    console.log(obj);
    return res.error({...obj})
  }

  if (err.name === "ValidationError") {
    return res.error({code:400, error:err})
  }

  if (err.name === "UnauthorizedError") {
    // jwt authentication error
    return res.error({code:400, message:" Invalid Token",error:err})
  }

  // default to 500 server error
  return res.error({code:500, message: err.message })
}
