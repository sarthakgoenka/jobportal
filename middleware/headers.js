var etag = require('etag')
module.exports=function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    res.header("X-Frame-Options", "deny");
    res.header("X-Content-Type-Options", "nosniff");
    res.header("Access-Control-Allow-Methods", "DELETE, PUT, GET, POST");
    res.header("Cache-Control", "no-cache");
    res.header('Last-Modified', new Date());
    // res.setHeader('ETag', etag(body))
    next();
}
