module.exports = responseHandler;
function responseHandler(req, res, next) {
    res.success = ({ code, message, data }) => {
        res.status(code).send({ code, message, data });
        }
        res.error = ({ code, message = "Some error occured", error}) => {
        res.status(code).send({ code, message, error});
        }
        next();
}
