'use strict'
const Joi = require('joi');
module.exports = {

  postingJob: {
    body: Joi.object({
        title: Joi.string().min(6).max(128).required(),
        description:Joi.string().min(10).max(128).required(),
        jobtype: Joi.string().min(6).max(128).required(),
        companyname: Joi.string().min(2).max(128).required(),
    })
  },
}
