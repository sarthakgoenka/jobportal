'use strict'
const Joi = require('joi');
const roles = ["recruiter", "seeker"];
module.exports = {
  login: {
    body: Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(128).required()
    })
  },
  create: {
    body: Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().min(6).max(128).required(),
      role: Joi.any().valid(...roles).required(),
      name: Joi.string().max(128).required().required()
    })
  },
  reset: {
    body: Joi.object({
      email: Joi.string().email().required(),
      password1: Joi.string().min(6).max(128).required(),
      token: Joi.string().required(),
      password2: Joi.string().min(6).max(128).required(),
    })
  },
  forgot: {
    body: Joi.object({
      email: Joi.string().email().required()
    })
  }
}
