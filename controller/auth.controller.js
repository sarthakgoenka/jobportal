const { table } = require("console");
const authService = require("../services/auth.service");

exports.signup = (req, res, next) => {
  authService
    .signup(req.body.email,req.body.password, req.body.name, req.body.role)
    .then((user) => res.success({ code: 201, message: 'Created Successfully!', data:{...user.dataValues}}))
    .catch((err) => res.error({code:409, error:"EmailID Already exists"}))
};

exports.login = (req, res, next) => {
  authService
    .login(req.body)
    .then((user) =>
      user
        ? res.success({code:200, message:"Login Successfully!", data:{...user}})
        : res.error({code:400, error:"EmailID or password is incorrect"})
    )
    .catch((err) => {
      console.log(err);
      next(err);
    });
};

exports.deleteUser = (req, res, next) => {
  id = req.params.id;
  authService
    .deleteUser(id)
    .then((result) => res.success({ code: 204, message: 'User Deleted Successfully!'}))
    .catch((err) => {
      next(err);
    });
};

exports.resetPassword = (req, res,next) => {
  authService.resetPassword(req.body.email, req.body.password1, req.body.password2, req.body.token)
  .then((result) => res.success({ code: 200, message: 'Password reset Sucessfully, Please login with your new password'}))
  .catch(err=>{
    next(err)
  })
};

exports.forgotPassword = (req, res, next) => {
  authService.forgotPassword(req.body.email)
  .then((result) => res.success({ code: 200, message: 'Reset Link has been sent to your mail :)'}))
    .catch(err=>{
      console.log(err);
      next(err)
    })
};

exports.getCandidates = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
	authService.findCandidates(limit, page)
    .then(candidates => {
      res.success({code:200, message:"All Candidates", data:{...candidates}})
    })
    .catch((err) => {
		next(err)
    });
};


exports.getRecruiter = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
	authService.findRecruiter(limit, page)
    .then(recruiter => {
      res.success({code:200, message:"All Recruiters", data:{...recruiter}})
    })
    .catch((err) => {
		next(err)
    });
};

exports.deleteUser = (req, res, next) => {
  authService
    .deleteUser(req.params.id)
    .then((result) => res.success({ code: 204, message: 'User Deleted Sucessfully'}))
    .catch((err) => {
      next(err);
    });
};