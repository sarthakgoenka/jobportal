const Applications = require('../models/index').Application;
const { application } = require('express');
const applicationService = require("../services/application.service");


exports.applications = (req, res,next) => {
  const userid = req.payload.id;
  const { createdby, jid } = req.body;
  applicationService.applyToJob(userid, jid, createdby)
  .then((result) => res.success({ code: 201, message: 'Successfully Applied for this Job', data:{isApplied: true}}))
    .catch(e=>{
      if(e==='Already Applied for this Job'){
        res.error({code:422, error:"Already Applied for this Job", message:"Some Error Occured"})
      }else{
        next(e)
      }
    })
}

