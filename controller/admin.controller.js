const db = require("../models/index");
const { v4: uuidv4 } = require("uuid");

const adminService = require("../services/admin.service");
const jobService = require("../services/job.service");

exports.login = (req, res, next) => {
  adminService
    .login(req.body)
    .then((user) =>
      user
        ? res.success({code:200, message:"Login Successfully!", data:{...user}})
        : res.error({code:400, error:"EmailID or password is incorrect"})
    )
    .catch((err) => {
      console.log(err);
      next(err);
    });
};


exports.viewApplication = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
  const jobid = req.params.id;
    adminService
    .getApplicants(jobid, limit, page)
    .then((result) => {
      res.success({code:200, message:"All Applications", data:{...result}})

    })
    .catch((err) => {
      console.log(err)
      next(err);
    });
};

exports.getAppliedJobs = (req,res,next)=>{
    const limit = parseInt(req.query.limit);
    const page = parseInt(req.query.page);
    const userid = req.params.id;
    jobService
      .getAllAppliedJobs(userid, limit, page)
      .then((result) => {
        res.success({code:200, message:"All Applied Jobs", data:{...result}})

      })
      .catch((err) => {
        next(err);
      });
  }
