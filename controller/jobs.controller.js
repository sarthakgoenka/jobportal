const db = require("../models/index");
const User = db.User;
const JobOpening = db.Job;
let middleware = require("../middleware/verifyToken");
const Applications = db.Application;
const { v4: uuidv4 } = require("uuid");
const transformer = require('../transformers/transformer')
const jobService = require("../services/job.service");
const { nextTick } = require("process");
const { create } = require("domain");
exports.addOpening = (req, res, next) => {
  let { title, jobtype, description, companyname } = req.body;
  const uuid = uuidv4();
  jobService
    .createJob(title, jobtype, description, companyname, req.payload.id ,uuid)
    .then((result) => res.success({ code: 201, message: 'Job has been added Sucessfully', data:result}))
    .catch((err) => {
      next(err);
    });
};

exports.getJobs = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
  jobService
    .getAllJobs(limit, page)
    .then((jobs) => 
    res.success({code:200, message:"All jobs", data:{...jobs}})
    )
    .catch((err) => {
      next(err);
    });
};

exports.getJob = (req, res, next) => {
  const uuid = req.params.id;
  jobService
    .getJobById(uuid)
    .then((jobs) => 
    res.success({code:200, message:"Single Job", data:{...jobs}})
    )
    .catch((err) => {
      next(err);
    });
};

exports.getPostedJobs = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
  const createdby = req.payload.id;
  jobService
    .getAllJobsByRecruiter(createdby,limit, page)
    .then((result) => {
      res.success({code:200, message:"All Posted Jobs", data:{...result}})

    })
    .catch((err) => {
      next(err);
    });
};
exports.getAppliedJobs = (req,res,next)=>{
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
  const userid = req.payload.id;
  jobService
    .getAllAppliedJobs(userid, limit, page)
    .then((result) => {
      res.success({code:200, message:"All Applied Job", data:{...result}})

    })
    .catch((err) => {
      next(err);
    });
}

exports.viewApplication = (req, res, next) => {
  const limit = parseInt(req.query.limit);
  const page = parseInt(req.query.page);
  const jobid = req.params.id;

  const createdby = req.payload.id;
  jobService
    .getApplicants(jobid, createdby, limit, page)
    .then((result) => {
      res.success({code:200, message:"All Applications", data:{...result}})

    })
    .catch((err) => {
      console.log(err)
      next(err);
    });
};

exports.deleteJob = (req, res, next) => {
  jobService
    .deleteJob(req.params.id)
    .then((result) => res.success({ code: 204, message: 'Job Deleted Sucessfully'}))
    .catch((err) => {
      next(err);
    });
};
