var express = require('express');
var app = express();
require('dotenv').config();
var bodyParser = require('body-parser');
const sqldb = require('./config/sqlDB');
const cors = require('cors')
const helmet = require('helmet')
const apiRouter = require('./routes/api')
const expressValidator = require('express-validator');
const errorHandler = require('./middleware/error-handler');
const responseHandler = require('./middleware/responseHandler');
const headers=require('./middleware/headers');
const rateLimit = require("express-rate-limit");

try {
  sqldb.authenticate();
  console.log("Connection has been established successfully.");
} catch (error) {
  console.error("Unable to connect to the database:", error);
}
const limiter = rateLimit({
  windowMs: 60 * 1000,
  max: 60
 });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(helmet());
app.use(headers);
app.use(limiter);
app.use(cors());
app.use(responseHandler);
app.use('/api/v1', apiRouter);
app.use(errorHandler);
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server started at port ${PORT}`));
