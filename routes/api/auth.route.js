'use strict'

const express = require('express')
const router = express.Router()
const authController = require('../../controller/auth.controller')
const { validate} = require('express-validation')
const { login, create, reset, forgot} = require('../../validators/user.validator')
const middleware = require('../../middleware/verifyToken');


router.post('/login', validate(login),authController.login)
router.post('/signup',validate(create),  authController.signup) 
router.post('/reset-password', validate(reset), authController.resetPassword) 
router.post('/forgot-password', validate(forgot), authController.forgotPassword) 

router.delete('/:id',middleware.verifyAdminToken, authController.deleteUser);
module.exports = router