'use strict'

const express = require('express')
const router = express.Router()
const authRouter = require('./auth.route')
const job = require('./jobs.route');
const applications = require('./application.route')
const admin = require('./admin.route');

router.use('/auth', authRouter);
router.use('/jobs', job);
router.use('/application',applications);
router.use('/admin',admin);
module.exports = router