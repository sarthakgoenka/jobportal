'use strict'

const express = require('express')
const router = express.Router()
const authController = require('../../controller/auth.controller')
const jobController = require('../../controller/jobs.controller')
const adminController = require('../../controller/admin.controller')
const middleware = require('../../middleware/verifyToken');
const { validate} = require('express-validation')
const { login} = require('../../validators/user.validator')


router.post('/login', validate(login),adminController.login)
router.get('/jobs',middleware.verifyAdminToken ,jobController.getJobs)
router.get('/candidates',middleware.verifyAdminToken ,authController.getCandidates)
router.get('/recruiters',middleware.verifyAdminToken,authController.getRecruiter)
router.get('/jobs/:id/applications',middleware.verifyAdminToken , adminController.viewApplication)
router.get('/candidates/:id/jobs',middleware.verifyAdminToken , adminController.getAppliedJobs)

module.exports = router