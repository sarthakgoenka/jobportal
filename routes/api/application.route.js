'use strict'

const express = require('express')
const router = express.Router()
const applicationController = require('../../controller/application.controller')
const middleware = require('../../middleware/verifyToken')
const { application } = require('../../validators/application.validator')
const { validate} = require('express-validation')

router.post("/",middleware.verifyToken, applicationController.applications);

module.exports = router