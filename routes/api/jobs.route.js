'use strict'

const express = require('express')
const router = express.Router()
const jobsController = require('../../controller/jobs.controller');
const applicationController = require('../../controller/application.controller')

const middleware = require('../../middleware/verifyToken');
const { validate} = require('express-validation')
const { postingJob} = require('../../validators/jobs.validator');
const { application } = require('../../validators/application.validator')


router.get('/', jobsController.getJobs); //To get all Jobs
router.get('/:id/applications', middleware.verifyEmployerToken, jobsController.viewApplication); // to get list of candidates for a job posted by a recruiter
router.get('/posted-job',middleware.verifyEmployerToken, jobsController.getPostedJobs) //To get all the Posted jobs for a Recruiter with Recruiter ID
router.get('/applied-job', middleware.verifyCandidateToken,jobsController.getAppliedJobs) //To get  all the Applied jobs for a Candidate with his ID
router.get('/:id', jobsController.getJob); //To get a Particular job with JobID


router.post('/', validate(postingJob), middleware.verifyEmployerToken, jobsController.addOpening); // Create a New Job Opening
// router.post('/:id',middleware.verifyToken, applicationController.applications) //For applying a Particuar Job

router.delete('/:id',middleware.verifyAdminToken,jobsController.deleteJob); //To gdelete a Particular Job for **ADMIN**

module.exports = router