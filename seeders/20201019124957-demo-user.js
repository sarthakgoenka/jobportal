module.exports = {
  up: async (queryInterface, Sequelize) => {
  /**
  * Add seed commands here.
  *
  * Example:
  */
  await queryInterface.bulkInsert('Users', [{
  name: 'Admin',
  uuid: '292a86ab-17ef-43ae-9bba-30be719308d2',
  id: 0,
  email: 'admin@mail.com',
  password: '$2y$08$oIEQIywqcJj9sxLsQf54V.IzsD95aE7..oSsV0s8guzKPonaxWbpq',
  role:'admin',
  createdAt: new Date(),
  updatedAt: new Date(),
  }], {});
  },
  
  down: async (queryInterface, Sequelize) => {
  /**
  * Add commands to revert seed here.
  *
  * Example:
  * await queryInterface.bulkDelete('People', null, {});
  */
  }
 };