const JobRepo = require("../repos/job.repo");
const transformer = require("../transformers/transformer");
const Job = require("../models/index").Job;
const Application = require("../models/index").Application;
const User = require("../models/index").User;
const { v4: uuidv4 } = require('uuid');

module.exports = {
  getAllJobs,
  getJobById,
  createJob,
  getApplicants,
  getAllJobsByRecruiter,
  deleteJob,
  getAllAppliedJobs,
};

jobObject = new JobRepo(Job);
applicationObject = new JobRepo(Application);
userObject = new JobRepo(User);


async function getAllJobs(limit, page) {
  const offset = +((+page-1)*(+limit))
  let jobs = await jobObject.findByConditionUsingOffset({
    limit: limit,
    offset: offset,
    where: {}
  });
  const metadata = {
    "resultset": {
      "count": jobs.result.count,
      "offset": jobs.result.offset,
      "limit": jobs.result.limit
    }
  }
  jobs.result.rows.map((job) => {
    transformer.commonTransformer(job.dataValues);
    return job;
  });
  return {
    "metadata": metadata,
    "results": jobs.result.rows
  };
  
}

async function createJob(
  title,
  jobtype,
  description,
  companyname,
  createdby,
  uuid
) {
console.log(uuid);
  let data =  await jobObject.create({
    title,
    jobtype,
    description,
    companyname,
    createdby,
    uuid
  });
  transformer.commonTransformer(data.result.dataValues);
  return data
}
async function getJobById(uuid) {
  let data = await jobObject.findOneByCondition({ uuid: uuid });
  data.result.map((job) => {
    transformer.commonTransformer(job.dataValues);
    return job;
  });
  return data
}
async function getAllJobsByRecruiter(createdby, limit, page) {
  // let data = await jobObject.findByCondition({ createdby: createdby });
  const offset = +((+page-1)*(+limit))
  let data = await jobObject.findByConditionUsingOffset({
    limit,
    offset,
   where: { createdby: createdby }
  });
  const metadata = {
    "resultset": {
      "count": data.result.count,
      "offset": data.result.offset,
      "limit": data.result.limit
    }
  }
  data.result.rows.map((d) => {
    transformer.commonTransformer(d.dataValues);
    return d;
  });
  return {
    "metadata": metadata,
    "results": data.result.rows
  }
}
async function getApplicants(jobid, createdby, limit, page) {
  const offset = +((+page-1)*(+limit))
  let applicants = [];
  await jobObject
    .findApplications(
      {
        jobid: jobid,
        createdby: createdby,
      },
      "userid"
    )
    .then((result) => {
      for (let applicant of result.result) {
        applicants.push(applicant.dataValues.userid);
      }
    });
  let data = await userObject.findByConditionUsingOffset({
    limit,offset,
    where:{ uuid: applicants }
  });
  const metadata = {
    "resultset": {
      "count": data.result.count,
      "offset": data.result.offset,
      "limit": data.result.limit
    }
  }
  data.result.rows.map((applicant) => {
    transformer.commonTransformer(applicant.dataValues);
    return applicant;
  });
  return {
    "metadata": metadata,
    "results": data.result.rows
  }
}
async function deleteJob(uuid) {
  return await jobObject.deleteByCondition({ uuid: uuid });
}

async function getAllAppliedJobs(userid, limit, page) {
  const offset = +((+page-1)*(+limit))
  let jobs = [];
  await jobObject
    .findApplications(
      {
        userid: userid,
      },
      "jobid"
    )
    .then((result) => {
      console.log(result)
      for (let job of result.result) {
        jobs.push(job.dataValues.jobid);
      }
    });
    console.log(jobs);
  let data = await jobObject.findByConditionUsingOffset({
    limit, offset,
    where:{ uuid: jobs }
  });
  const metadata = {
    "resultset": {
      "count": data.result.count,
      "offset": data.result.offset,
      "limit": data.result.limit
    }
  }
  data.result.rows.map((job) => {
    transformer.commonTransformer(job.dataValues);
    return job;
  });
  return {
    "metadata": metadata,
    "results": data.result.rows
  }}
