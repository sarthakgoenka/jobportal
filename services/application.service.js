const ApplicationRepo = require("../repos/application.repo");
const Application = require("../models/index").Application;
const User = require("../models/index").User;
const mailService = require('../services/mail.service');

module.exports = {
    applyToJob
};

applicationObject = new ApplicationRepo(Application);
userObject = new ApplicationRepo(User);


async function applyToJob(userid, jobid, createdby){
    if (await applicationObject.findOneByCondition({userid,jobid})) {
        throw 'Already Applied for this Job';
      }
    let application =  await applicationObject.create({userid,jobid,createdby});
    if(application){
    let user = await userObject.findOneByCondition({uuid:userid});
    let userEmail = user.dataValues.email;
    let recruiter = await userObject.findOneByCondition({uuid:createdby});
    let recruiterEmail = recruiter.dataValues.email;
    mailService.sendMail(userEmail, 'Job Application', "You application for job is on the way :)");
    mailService.sendMail(recruiterEmail, 'Job Application', user.dataValues.name+" has applied to a job that you posted");

    }
    return application;

}