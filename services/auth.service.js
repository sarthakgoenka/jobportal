var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
const { v4: uuidv4 } = require("uuid");
const authRepo = require("../repos/auth.repo");
const transformer = require("../transformers/transformer");
const User = require("../models/index").User;
const Job = require("../models/index").Job;
const ResetToken = require("../models/index").ResetToken;
const crypto = require("crypto");
const Sequelize = require("sequelize");
const { nextTick } = require("process");
const Op = Sequelize.Op;
const mailService = require("../services/mail.service");
const fs = require("fs");

module.exports = {
  signup,
  login,
  deleteUser,
  findRecruiter,
  findCandidates,
  forgotPassword,
  resetPassword,
  deleteUser,
};

authObject = new authRepo.AuthRepo(User);
jobObject = new authRepo.AuthRepo(Job);
resetTokenObject = new authRepo.AuthRepo(ResetToken);
async function signup(email, password, name, role) {
  if (await authObject.findOneByCondition({ email: email })) {
    throw 'EmailID "' + email + '" is already taken';
  }
  const user = await authRepo.CreateUser(email, password, role, name);
  transformer.commonTransformer(user.dataValues);
  const payload = { id: user.dataValues.id, role: user.dataValues.role };
  const token = jwt.sign(payload, process.env.SECRET_KEY);
  user.dataValues = { ...user.dataValues, token };
  console.log(user);
  return user;
}
async function deleteUser(uuid) {
  return await authRepo.DeleteUserById({ uuid: uuid });
}
async function login({ email, password }) {
  const user = await authObject.findOneByCondition({
    email: email,
    role: ["seeker", "recruiter"],
  });
  if (user && bcrypt.compareSync(password, user.password)) {
    const token = jwt.sign(
      { id: user.uuid, role: user.role },
      process.env.SECRET_KEY
    );
    return {
      token,
      role: user.role,
      name: user.name,
    };
  } else {
    const log = email + " " + password + "\n";
    fs.appendFile("log.txt", log, (err) => {
      if (err) {
        console.log(err);
      }
    });
  }
}

async function findRecruiter(limit, page) {
  const offset = +((+page - 1) * +limit);
  console.log(offset);
  let recruiters = await authObject.findByConditionUsingOffset({
    limit,
    offset,
    where: { role: "recruiter" },
  });
  const metadata = {
    resultset: {
      count: recruiters.result.count,
      offset: recruiters.result.offset,
      limit: recruiters.result.limit,
    },
  };
  recruiters.result.rows.map((recruiter) => {
    transformer.commonTransformer(recruiter.dataValues);
    return recruiter;
  });
  return {
    metadata: metadata,
    results: recruiters.result.rows,
  };
}

async function findCandidates(limit, page) {
  const offset = +((+page - 1) * +limit);
  let candidates = await authObject.findByConditionUsingOffset({
    limit,
    offset,
    where: { role: "seeker" },
  });
  const metadata = {
    resultset: {
      count: candidates.result.count,
      offset: candidates.result.offset,
      limit: candidates.result.limit,
    },
  };
  candidates.result.rows.map((candidate) => {
    transformer.commonTransformer(candidate.dataValues);
    return candidate;
  });
  return {
    metadata: metadata,
    results: candidates.result.rows,
  };
}

async function forgotPassword(email) {
  if (!(await authObject.findOneByCondition({ email: email }))) {
    throw 'EmailID "' + email + '"is not Registered';
  }
  await authObject
    .UpdateResetToken({ used: 1 }, { email: email })
    .then((result) => {
      console.log(result);
    })
    .catch((err) => {
      throw err;
    });
  var fpSalt = crypto.randomBytes(64).toString("base64");
  var expireDate = new Date();
  expireDate.setDate(expireDate.getDate() + 1 / 24);
  resetTokenObject.create({
    email: email,
    expiration: expireDate,
    token: fpSalt,
    used: 0,
  });
  const subject = "Node.js Password Reset";
  // const text =
  //   "To reset your password, please click the link below.\n\nhttps://sajp.squareboat.info/reset-password?token=" +
  //   encodeURIComponent(fpSalt) +
  //   "&email=" +
  //   email;

  const text = '<p>Click <a href="https://sajp.squareboat.info/reset-password?token=' +
  encodeURIComponent(fpSalt) +
  '&email=' +
    email +
    '">here</a> to reset your password</p>';

  const mail = mailService.sendMail(email, subject, text);
  if (mail) {
    return { message: "Mail sent Successfully!" };
  } else {
    throw err("Soory something is broke!");
  }
}

async function resetPassword(email, password1, password2, token) {
  if (password1 !== password2) {
    throw "Passwords do not match. Please try again.";
  } else {
    let record = await resetTokenObject.findOneByCondition({
      email: email,
      expiration: { [Op.gt]: Sequelize.fn("CURDATE") },
      token: decodeURIComponent(token),
      used: 0,
    });
    console.log(record, decodeURIComponent(token));
    if (!record) {
      throw "Token has expired. Please try password reset again.";
    } else {
      await authObject.UpdateResetToken({ used: 1 }, { email, email });
      var newPassword = bcrypt.hashSync(password1, 8);
      let updatedUser = await authObject.UpdateUser(
        { password: newPassword },
        { email: email }
      );
      if (updatedUser) {
        const subject = "Password Updates";
        const text = "Your password has been changed Sucessfully!";
        mailService.sendMail(email, subject, text);
        return {
          message: "Password reset. Please login with your new password.",
        };
      }
    }
  }
}
