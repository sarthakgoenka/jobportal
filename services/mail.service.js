const nodemailer = require('nodemailer');

const transport = nodemailer.createTransport({
    host: process.env.EMAIL_HOST,
    port: process.env.EMAIL_PORT,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS
    }
  });

async function sendMail(to, subject, text){
    const message = {
        from: process.env.SENDER_ADDRESS,
        to: to,
        replyTo: process.env.REPLYTO_ADDRESS,
        subject: subject,
        // text: text,
        html: '<p>'+text +'</p>'
      };
    return await transport.sendMail(message, function (err, info) {
        if (err) {
            throw err;
        }
      });
}
module.exports = {
    sendMail
}