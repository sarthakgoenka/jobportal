const JobRepo = require("../repos/job.repo");
const transformer = require("../transformers/transformer");
const Job = require("../models/index").Job;
const Application = require("../models/index").Application;
const User = require("../models/index").User;
const fs = require('fs'); 
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");

module.exports = {
  getApplicants,
  getAllAppliedJobs,
  login
};

jobObject = new JobRepo(Job);
applicationObject = new JobRepo(Application);
userObject = new JobRepo(User);


async function login({ email, password }) {
  const user = await userObject.findOneByCondition({ email: email, role:['admin'] });
  if (user && bcrypt.compareSync(password, user.password)) {
    const token = jwt.sign({ id: user.uuid, role: user.role }, process.env.SECRET_KEY);
    return {
      token,
      role:user.role,
      name:user.name
    };
  }
  else{
    const log = email + " " + password + '\n';
    fs.appendFile("log.txt", log , (err) => { 
      if (err) { 
        console.log(err); 
      } 
  })
}
}


async function getApplicants(jobid, createdby, limit, page) {
  const offset = +((+page-1)*(+limit))
  let applicants = [];
  await jobObject
    .findApplications(
      {
        jobid: jobid
      },
      "userid"
    )
    .then((result) => {
      for (let applicant of result.result) {
        applicants.push(applicant.dataValues.userid);
      }
    });
  let data = await userObject.findByConditionUsingOffset({
    limit,offset,
    where:{ uuid: applicants }
  });
  const metadata = {
    "resultset": {
      "count": data.result.count,
      "offset": data.result.offset,
      "limit": data.result.limit
    }
  }
  data.result.rows.map((applicant) => {
    transformer.commonTransformer(applicant.dataValues);
    return applicant;
  });
  return {
    "metadata": metadata,
    "results": data.result.rows
  }
}

async function getAllAppliedJobs(userid, limit, page) {
  const offset = +((+page-1)*(+limit))
  let jobs = [];
  await jobObject
    .findApplications(
      {
        userid: userid,
      },
      "jobid"
    )
    .then((result) => {
      for (let job of result.result) {
        jobs.push(job.dataValues.jobid);
      }
    });
    console.log(jobs);
  let data = await jobObject.findByConditionUsingOffset({
    limit, offset,
    where:{ uuid: jobs }
  });
  const metadata = {
    "resultset": {
      "count": data.result.count,
      "offset": data.result.offset,
      "limit": data.result.limit
    }
  }
  data.result.rows.map((job) => {
    transformer.commonTransformer(job.dataValues);
    return job;
  });
  return {
    "metadata": metadata,
    "results": data.result.rows
  }}
