const User = require('../models/index').User;
const Job = require('../models/index').Job;
const { v4: uuidv4 } = require('uuid');
var bcrypt = require('bcryptjs');
const ResetToken = require("../models/index").ResetToken;



async function CreateUser(email,password, role, name) {
  console.log("name", name, "role", role);
    return await User.create({
		name: name,
		email: email,
		role: role,
		uuid: uuidv4(),
    password: bcrypt.hashSync(password, 8)})
}

 async function DeleteUserById (condition){
        return await User.destroy({where:condition})
        .then(data => {
            if (data === 1) {
                Job.destroy({where:{createdby:condition.uuid}}).then(data=>{
                   return {
                    result: "Sucessfully deleted!"
                }
                })

            } else {
                return {
                    result: null
                }
            }
        })
    }
const BaseRepo = require('./base.repo');
class AuthRepo extends BaseRepo {
    constructor(model) {
        super(model);
    }
    async UpdateResetToken(updation,condition){
      return await ResetToken.update(updation,{where:condition});
    }

    async UpdateUser(updation, condition){
      return await User.update(updation, {where:condition});
    }
}
module.exports = {
  CreateUser,
  DeleteUserById,
  AuthRepo
 };