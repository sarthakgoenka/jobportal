const { v4: uuidv4 } = require('uuid');
const { CreateUser } = require('./auth.repo');
const Application = require("../models/index").Application;




const BaseRepo = require('./base.repo');
class JobRepo extends BaseRepo {
    constructor(model) {
        super(model);
    }
    async findApplications(condition, entry){
        return await Application.findAll({where:condition, attributes:[entry]})
        .then(result => {
                return {
                    result: result
                }
        })
    }
   
}
module.exports = JobRepo;