const {User} = require('../models/index');
const jwt = require('jsonwebtoken');
const transformers =require('../transformers/transformer');
class BaseRepo {
    constructor(model) {
        this.model = model;
    }
    async create(details) {
        return await this.model.create(details)
            .then(result => {
                return {
                    isCreated: true,
                    result: result
                }
            })
            .catch(err => {
                return {
                    isCreated: false,
                    result: err
                }
            })
    }

    async findUser(userDetails) {
        return await User.findOne({
                where: {
                    email: userDetails.email
                }
            })
            .then(user => {
                    return {
                        result: user
                    }
            })
    }

    async findByCondition(condition){
        return await this.model.findAll({where:condition})
        .then(result => {
                return {
                    result: result
                }
        })
    }
    async findAll(){
        return await this.model.findAll()
        .then(result => {
                return {
                    result: result
                }
        })
    }
    async findOneByCondition(condition){
        console.log(this.model);
        return await this.model.findOne({where:condition});
    }
    async deleteByCondition(condition){
        return await this.model.destroy({where:condition})
        .then(data => {
            if (data === 1) {
                return {
                    result: "Sucessfully deleted!"
                }
            } else {
                return {
                    result: null
                }
            }
        })
    }
    async findByConditionUsingOffset(condition){
        // console.log('cond',condition)
        let newCondition={where:{...condition.where}}
        let totalUser=await this.model.count(newCondition);
        let offsetpassed=condition.offset;
        let limitPassed=condition.limit;
        if(offsetpassed<0 || !offsetpassed){
            offsetpassed=0;
        }
        if(limitPassed<0){
            limitPassed=0;
        }
        if(!limitPassed){
            limitPassed = totalUser;
        }
        if(offsetpassed>totalUser){
            offsetpassed=0;
        }
        condition.offset=offsetpassed;
        condition.limit=limitPassed;

        return await this.model.findAndCountAll(condition)
        .then(data => {
            console.log('data',data);
               data.offset=limitPassed+offsetpassed;
               data.limit=limitPassed;
                if(data.offset>totalUser){
                    data.offset=-1;
                }
                return {
                    result: data
                }   
        })
    }
}
module.exports = BaseRepo;