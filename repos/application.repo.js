const Application = require('../models/index').Application;
const User = require('../models/index').User;


const BaseRepo = require('./base.repo');
class ApplicationRepo extends BaseRepo {
    constructor(model) {
        super(model);
    }
   
}
module.exports = ApplicationRepo